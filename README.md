# README

You can see the app working on:
https://gresb.herokuapp.com/mushrooms?bruises=t&cap-color=b&cap-surface=s&edible=p&gill-attachment=f&gill-color=h&gill-size=b&habitat=g&population=s&ring-number=o&stalk-shape=t&stalk-surface-above-ring=s&stalk-surface-below-ring=s&veil-color=w&veil-type=p

# Background


This repo is the result of a short assessment for a Ruby on Rails role I did myself back in 2017.
We have used this several times to assess the skills of the applicant.
This is the accompanying text we supplied:

For your assessment, I would like to ask you to look at the following dataset: https://archive.ics.uci.edu/ml/datasets/Mushroom
Specifically, it is about the files agaricus-lepiota.data and agaricus-lepiota.names on https://archive.ics.uci.edu/ml/machine-learning-databases/mushroom/.

Remember, this is not an assessment you can fail, but I would just like to see how you would approach a problem like this.
Also, don't feel like you should finish the whole thing. Just timebox yourself and don't spend more than 2-3 hours on it.

What I would like to ask you, is to create a small app in which you can filter out mushrooms that meet specific attributes.
For example, I would like to see a list of all mushrooms that are edible, have a brown gill-color, and two rings.
The dataset contains 8124 types of mushrooms, each with 23 different attributes:

- edible
- cap-shape
- cap-surface
- cap-color
- bruises?
- odor
- gill-attachment
- gill-spacing
- gill-size
- gill-color
- stalk-shape
- stalk-root
- stalk-surface-above-ring
- stalk-surface-below-ring
- stalk-color-above-ring
- stalk-color-below-ring
- veil-type
- veil-color
- ring-number
- ring-type
- spore-print-colororange
- population
- habitat

This would include a front-end that enables me to filter out the mushrooms that have one of more of the attributes that I can select.
Also I would like to be able to reset the filters. This means you will have to:

- Import and process the dataset
- Make model/views/controller choices
- Make smart database queries and a minimal front-end
- Optionally, hosting considerations and front-end design choices.
