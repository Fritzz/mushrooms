class MushroomsController < ApplicationController

  def index
    @attr_options = Mushroom.attr_options
    @attributes = @attr_options.keys
    @filter = {}

    mushroom_params.each do |key, value|
      @filter[key] = value if value.in? @attr_options[key].keys
    end
    
    if @filter.present?
      @mushrooms = Mushroom.where(@filter)
    else
      @mushrooms = Mushroom.all
    end
  end

  private

  def mushroom_params
    params.permit(@attributes)
  end


end
