require 'csv'

class DataImporter

  def import
    data = CSV.open(Rails.root + 'public/agaricus-lepiota.data')
    headers = ["edible",
    "cap-shape",
    "cap-surface",
    "cap-color",
    "bruises",
    "odor",
    "gill-attachment",
    "gill-spacing",
    "gill-size",
    "gill-color",
    "stalk-shape",
    "stalk-root",
    "stalk-surface-above-ring",
    "stalk-surface-below-ring",
    "stalk-color-above-ring",
    "stalk-color-below-ring",
    "veil-type",
    "veil-color",
    "ring-number",
    "ring-type",
    "spore-print-color",
    "population",
    "habitat"]

    data.each do |line|
      attributes = headers.zip(line).to_h
      Mushroom.create!(attributes)
    end
  end
end