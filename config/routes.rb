Rails.application.routes.draw do

  root to: redirect('/mushrooms/?bruises=t&cap-color=b&cap-surface=s&edible=p&gill-attachment=f&gill-color=h&gill-size=b&habitat=g&population=s&ring-number=o&stalk-shape=t&stalk-surface-above-ring=s&stalk-surface-below-ring=s&veil-color=w&veil-type=p')
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :mushrooms
end
